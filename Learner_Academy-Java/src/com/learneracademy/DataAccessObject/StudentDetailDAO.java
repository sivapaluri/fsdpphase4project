package com.learneracademy.DataAccessObject;

import java.util.List;

import com.learneracademy.model.StudentDetail;

public interface StudentDetailDAO {
	

	public StudentDetail createStudentDetail(StudentDetail studentdetail);

	public List<StudentDetail> getStudentDetails();

	public StudentDetail updateStudentDetail(StudentDetail studentdetail);

	public StudentDetail getStudentDetailById(int id);

	public void removeStudentDetail(int id);

}
