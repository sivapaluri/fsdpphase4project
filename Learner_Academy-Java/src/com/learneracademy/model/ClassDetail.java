package com.learneracademy.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Class")
public class ClassDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private int classId;

	private String className;

// One class can have many Students

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "classId")
//@JoinTable(name = "CLASS_STUDENT", joinColumns = { @JoinColumn(name = "classId") }, inverseJoinColumns = { 
//	@JoinColumn(name = "id") })

	private Set<StudentDetail> students = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "classId")
	private Set<SubjectDetail> subjects = new HashSet<>();

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name = "classId")
	private Set<TeacherDetail> teacher = new HashSet<>();

	public ClassDetail() {
		// TODO Auto-generated constructor stub
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Set<StudentDetail> getStudents() {
		return students;
	}

	public void setStudents(Set<StudentDetail> students) {
		this.students = students;
	}

	public Set<SubjectDetail> getSubjects() {
		return subjects;
	}

	public void setSubjects(Set<SubjectDetail> subjects) {
		this.subjects = subjects;
	}

	public Set<TeacherDetail> getTeacher() {
		return teacher;
	}

	public void setTeacher(Set<TeacherDetail> teacher) {
		this.teacher = teacher;
	}

	public ClassDetail(String className, Set<StudentDetail> students, Set<SubjectDetail> subjects,
			Set<TeacherDetail> teacher) {
		super();
		this.className = className;
		this.students = students;
		this.subjects = subjects;
		this.teacher = teacher;
	}

}