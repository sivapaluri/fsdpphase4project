package com.learneracademy.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Student")
public class StudentDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	// @Column(name = "STUDENT_NAME")
	private String studentName;
	// @Column(name = "STUDENT_GENDER")
	private String studentGender;
	// @Column(name = "STUDENT_AGE")

	private int studentAge;

//	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@JoinColumn(name = "classId")
//	private ClassDetail studentClass;

	public StudentDetail() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentGender() {
		return studentGender;
	}

	public void setStudentGender(String studentGender) {
		this.studentGender = studentGender;
	}

	public int getStudentAge() {
		return studentAge;
	}

	public void setStudentAge(int studentAge) {
		this.studentAge = studentAge;
	}

	public StudentDetail(int id, String studentName, String studentGender, int studentAge) {
		super();
		this.id = id;
		this.studentName = studentName;
		this.studentGender = studentGender;
		this.studentAge = studentAge;
	}

}
