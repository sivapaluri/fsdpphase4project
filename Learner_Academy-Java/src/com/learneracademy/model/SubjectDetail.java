package com.learneracademy.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Subject")

public class SubjectDetail {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "SUB_SERIAL_NUMBER")
	private int sNo;

	@Column(name = "SUBJECT_CODE")
	private String subjectCode;
	@Column(name = "SUBJECT_NAME")
	private String subjectName;
	

//	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
//	@JoinColumn(name = "TEACHER_ID" ,nullable=false)
//	private TeacherDetail teacher;






	public SubjectDetail() {
		// TODO Auto-generated constructor stub
	}

	public int getsNo() {
		return sNo;
	}

	public void setsNo(int sNo) {
		this.sNo = sNo;
	}

	public String getSubjectCode() {
		return subjectCode;
	}

	public void setSubjectCode(String subjectCode) {
		this.subjectCode = subjectCode;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}


	public SubjectDetail(String subjectCode, String subjectName) {
		super();
		this.subjectCode = subjectCode;
		this.subjectName = subjectName;
	}





}
