package com.learneracademy.serviceImplementation;

import java.util.List;

import com.learneracademy.DataAccessObject.TeacherDetailDAO;
import com.learneracademy.DataAccessObjectImplementation.TeacherDetailDAOImpl;
import com.learneracademy.Service.TeacherDetailService;
import com.learneracademy.model.TeacherDetail;

public class TeacherDetailServiceImpl implements TeacherDetailService {
	
	TeacherDetailDAO teacherdetaildao = new TeacherDetailDAOImpl();

	@Override
	public TeacherDetail createTeacherDetail(TeacherDetail teacherdetail) {
		// TODO Auto-generated method stub
		return teacherdetaildao.createTeacherDetail(teacherdetail);
	}

	@Override
	public List<TeacherDetail> getTeacherDetails() {
		// TODO Auto-generated method stub
		return teacherdetaildao.getTeacherDetails();
	}

	@Override
	public TeacherDetail updateTeacherDetail(TeacherDetail teacherdetail) {
		// TODO Auto-generated method stub
		return teacherdetaildao.updateTeacherDetail(teacherdetail);
	}

	@Override
	public TeacherDetail getTeacherDetailById(int id) {
		// TODO Auto-generated method stub
		return teacherdetaildao.getTeacherDetailById(id);
	}

	@Override
	public  void removeTeacherDetail(int id) {
		// TODO Auto-generated method stub
		teacherdetaildao.removeTeacherDetail(id);
		
	}

}
