package com.learneracademy.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.learneracademy.Service.StudentDetailService;
import com.learneracademy.model.ClassDetail;
import com.learneracademy.model.StudentDetail;
import com.learneracademy.serviceImplementation.StudentDetailServiceImpl;

@Path("/studentdetail")

public class StudentDetailController {
	
StudentDetailService studentdetailservice = new StudentDetailServiceImpl();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public StudentDetail createStudent(StudentDetail studentdetail) {
		return studentdetailservice.createStudentdetail(studentdetail);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<StudentDetail> getStudentDetails() {
		return studentdetailservice.getStudentDetails();
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public StudentDetail updateStudentDetail(StudentDetail studentdetail) {
		return studentdetailservice.updateStudentDetail(studentdetail);
	}
	
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public StudentDetail getStudentDetailById(@PathParam("id") int id) {
		return studentdetailservice.getStudentDetailById(id);
	}
	
	@DELETE
	@Path("/{id}")
	public void removeStudentDetail(@PathParam("id") int id) {
		studentdetailservice.removeStudentDetail(id);
	}
	
	
	

}
