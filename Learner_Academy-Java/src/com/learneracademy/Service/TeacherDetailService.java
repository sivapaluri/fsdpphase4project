package com.learneracademy.Service;

import java.util.List;

import com.learneracademy.model.TeacherDetail;

public interface TeacherDetailService {

	public TeacherDetail createTeacherDetail(TeacherDetail teacherdetail);

	public List<TeacherDetail> getTeacherDetails();

	public TeacherDetail updateTeacherDetail(TeacherDetail teacherdetail);

	public TeacherDetail getTeacherDetailById(int id);

	public void removeTeacherDetail(int id);

}
