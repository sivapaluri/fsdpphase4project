import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { HomeComponent } from './components/home/home.component';
import { MissionComponent } from './components/mission/mission.component';
import { VisionComponent } from './components/vision/vision.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { AdmintasksComponent } from './components/admintasks/admintasks.component';
import { StudentTaskComponent } from './components/student-task/student-task.component';
import { AddstudentComponent } from './components/student-operations/addstudent/addstudent.component';
import { StudentListComponent } from './components/student-operations/student-list/student-list.component';
import { StudentByIdComponent } from './components/student-operations/student-by-id/student-by-id.component';
import { RemovestudentComponent } from './components/student-operations/removestudent/removestudent.component';
import { UpdatestudentComponent } from './components/student-operations/updatestudent/updatestudent.component';
import { StudentService } from './service/student.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AddsubjectComponent } from './components/subject-operations/addsubject/addsubject.component';
import { RemovesubjectComponent } from './components/subject-operations/removesubject/removesubject.component';
import { UpdatesubjectComponent } from './components/subject-operations/updatesubject/updatesubject.component';
import { SubjectByIdComponent } from './components/subject-operations/subject-by-id/subject-by-id.component';
import { SubjectlistComponent } from './components/subject-operations/subjectlist/subjectlist.component';
import { AddteacherComponent } from './components/teacher-operations/addteacher/addteacher.component';
import { RemoveteacherComponent } from './components/teacher-operations/removeteacher/removeteacher.component';
import { TeacherByIdComponent } from './components/teacher-operations/teacher-by-id/teacher-by-id.component';
import { TeacherlistComponent } from './components/teacher-operations/teacherlist/teacherlist.component';
import { UpdateteacherComponent } from './components/teacher-operations/updateteacher/updateteacher.component';
import { AddclassComponent } from './components/class-operations/addclass/addclass.component';
import { RemoveclassComponent } from './components/class-operations/removeclass/removeclass.component';
import { UpdateclassComponent } from './components/class-operations/updateclass/updateclass.component';
import { ClassByIdComponent } from './components/class-operations/class-by-id/class-by-id.component';
import { ClasslistComponent } from './components/class-operations/classlist/classlist.component';
import { ClassTaskComponent } from './components/class-task/class-task/class-task.component';
import { SubjectTaskComponent } from './components/subject-task/subject-task/subject-task.component';
import { TeacherTaskComponent } from './components/teacher-task/teacher-task/teacher-task.component';
import { ClassService } from './service/class.service';
import { TeacherService } from './service/teacher.service';
import { SubjectService } from './service/subject.service';


@NgModule({
  declarations: [
    AppComponent,
    AboutusComponent,
    HomeComponent,
    MissionComponent,
    VisionComponent,
    TestimonialsComponent,
    AdmintasksComponent,
    StudentTaskComponent,
    AddstudentComponent,
    StudentListComponent,
    StudentByIdComponent,
    RemovestudentComponent,
    UpdatestudentComponent,
    AddsubjectComponent,
    RemovesubjectComponent,
    UpdatesubjectComponent,
    SubjectByIdComponent,
    SubjectlistComponent,
    AddteacherComponent,
    RemoveteacherComponent,
    TeacherByIdComponent,
    TeacherlistComponent,
    UpdateteacherComponent,
    AddclassComponent,
    RemoveclassComponent,
    UpdateclassComponent,
    ClassByIdComponent,
    ClasslistComponent,
    ClassTaskComponent,
    SubjectTaskComponent,
    TeacherTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [StudentService,ClassService,TeacherService,SubjectService],
  bootstrap: [AppComponent]
})
export class AppModule { }
