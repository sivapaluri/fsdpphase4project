import { Student } from './student';
import { Subject } from './subject';
import { Teacher } from './teacher';




export class ClassDetail {

    classId:number;
    className:string;
    students:Student[];
    subjects:Subject[];
    teacher:Teacher[]; // students[0] && students[0].studentName
}
