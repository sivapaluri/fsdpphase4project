import {ClassDetail } from './classdetail';

export class Student {

     id:number;
     studentName:string;
     studentGender:string;
     studentAge:number;
     
}
