import { Subject } from './subject';

export class Teacher {

    private teacherID:number;
    private teacherName:string;
    private teacherContact:number;
    private subjects:Subject[];
   
}
