import { Component, OnInit } from '@angular/core';
import { TeacherService } from 'src/app/service/teacher.service';
import { Teacher } from 'src/app/AppModel/teacher';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addteacher',
  templateUrl: './addteacher.component.html',
  styleUrls: ['./addteacher.component.css']
})
export class AddteacherComponent implements OnInit {
private teacher:Teacher;
  constructor(private service:TeacherService, private router:Router) {
    this.teacher = new Teacher();
   }

   public createTeacher(){
     this.service.createTeacher(this.teacher).subscribe(data=>{
       this.teacher= new Teacher();
       this.router.navigate(["/teacherlist"]);
     })
   }

  ngOnInit() {
  }

}
