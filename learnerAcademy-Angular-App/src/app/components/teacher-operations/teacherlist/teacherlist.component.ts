import { Component, OnInit } from '@angular/core';
import { Teacher } from 'src/app/AppModel/teacher';
import { TeacherService } from 'src/app/service/teacher.service';

@Component({
  selector: 'app-teacherlist',
  templateUrl: './teacherlist.component.html',
  styleUrls: ['./teacherlist.component.css']
})
export class TeacherlistComponent implements OnInit {

  private teachers:Teacher[];
  constructor(private service:TeacherService) { }

  ngOnInit() {
    this.service.getAllteachers().subscribe(data=>{
      this.teachers=data;
      console.log(data);
    })
  }

}
