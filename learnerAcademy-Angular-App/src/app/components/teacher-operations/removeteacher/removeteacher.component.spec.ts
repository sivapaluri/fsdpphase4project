import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveteacherComponent } from './removeteacher.component';

describe('RemoveteacherComponent', () => {
  let component: RemoveteacherComponent;
  let fixture: ComponentFixture<RemoveteacherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveteacherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveteacherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
