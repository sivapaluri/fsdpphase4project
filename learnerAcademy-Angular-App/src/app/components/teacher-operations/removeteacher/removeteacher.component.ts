import { Component, OnInit } from '@angular/core';
import { TeacherService } from 'src/app/service/teacher.service';
import { Router } from '@angular/router';
import { Teacher } from 'src/app/AppModel/teacher';

@Component({
  selector: 'app-removeteacher',
  templateUrl: './removeteacher.component.html',
  styleUrls: ['./removeteacher.component.css']
})
export class RemoveteacherComponent implements OnInit {

  private teacherID:number;
  private teacher:Teacher;
  private teachers:Teacher[]=[];

  constructor(private service:TeacherService,private router:Router) { }

  public deleteTeacher(){
    return this.service.deleteTeacher(this.teacherID).subscribe(
      data=>{
        this.teacher=data;
        this.teachers.pop;
        this.router.navigate(['/teacherlist'])
      })
    }
  

  ngOnInit() {
  }

}
