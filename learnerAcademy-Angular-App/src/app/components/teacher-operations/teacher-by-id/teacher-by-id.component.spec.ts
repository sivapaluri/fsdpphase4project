import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherByIdComponent } from './teacher-by-id.component';

describe('TeacherByIdComponent', () => {
  let component: TeacherByIdComponent;
  let fixture: ComponentFixture<TeacherByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
