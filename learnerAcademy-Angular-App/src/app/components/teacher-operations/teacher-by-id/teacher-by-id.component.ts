import { Component, OnInit } from '@angular/core';
import { TeacherService } from 'src/app/service/teacher.service';
import { Teacher } from 'src/app/AppModel/teacher';

@Component({
  selector: 'app-teacher-by-id',
  templateUrl: './teacher-by-id.component.html',
  styleUrls: ['./teacher-by-id.component.css']
})
export class TeacherByIdComponent implements OnInit {
private teacherID:number;
private teacher:Teacher;
private teachers:Teacher[]=[];

  constructor(private service:TeacherService) { }

  public getTeacherById(){
    this.teachers=[];
    this.service.getTeacherById(this.teacherID).subscribe(data=>{
      this.teacher=data;
      this.teachers.push(this.teacher);
    })

  }

  ngOnInit() {
  }

}
