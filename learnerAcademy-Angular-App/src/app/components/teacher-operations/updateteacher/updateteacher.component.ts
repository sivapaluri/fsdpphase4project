import { Component, OnInit } from '@angular/core';
import { TeacherService } from 'src/app/service/teacher.service';
import { Teacher } from 'src/app/AppModel/teacher';

@Component({
  selector: 'app-updateteacher',
  templateUrl: './updateteacher.component.html',
  styleUrls: ['./updateteacher.component.css']
})
export class UpdateteacherComponent implements OnInit {
private teacher:Teacher;
  constructor(private service:TeacherService) {
    this.teacher = new Teacher();
   }


  public updateTeacher(){

    this.service.updateTeacher(this.teacher).subscribe(data=>{
      this.teacher=new Teacher();
    })

  }
  ngOnInit() {
  }

}
