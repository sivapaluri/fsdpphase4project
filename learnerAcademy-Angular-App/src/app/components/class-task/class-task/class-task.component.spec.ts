import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassTaskComponent } from './class-task.component';

describe('ClassTaskComponent', () => {
  let component: ClassTaskComponent;
  let fixture: ComponentFixture<ClassTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
