import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectTaskComponent } from './subject-task.component';

describe('SubjectTaskComponent', () => {
  let component: SubjectTaskComponent;
  let fixture: ComponentFixture<SubjectTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
