import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/service/student.service';
import { Student } from 'src/app/AppModel/student';
import { Router } from '@angular/router';

import { ClassService } from 'src/app/service/class.service';
import { ClassDetail } from 'src/app/AppModel/classdetail';

@Component({
  selector: 'app-addstudent',
  templateUrl: './addstudent.component.html',
  styleUrls: ['./addstudent.component.css']
})
export class AddstudentComponent implements OnInit {

  private student: Student;

  private id: number;
  constructor(private service: StudentService, private router: Router) {
    this.student = new Student();

  }

  createStudent() {



    this.service.createStudent(this.student).subscribe(data => {


      this.student = new Student();

      this.router.navigate(['/studentslist'])
    })



  }

  ngOnInit() {

  }

}
