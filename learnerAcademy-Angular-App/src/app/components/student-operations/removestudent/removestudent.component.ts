import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/service/student.service';
import { Student } from 'src/app/AppModel/student';
import { Router } from '@angular/router';

@Component({
  selector: 'app-removestudent',
  templateUrl: './removestudent.component.html',
  styleUrls: ['./removestudent.component.css']
})
export class RemovestudentComponent implements OnInit {

  private students:Student[]=[];
  private student:Student;
  private id:number;
  constructor(private service:StudentService, private router:Router) { }

  public deleteStudent(){

    return this.service.deleteStudent(this.id).subscribe(
      data=>{
        this.student=data;
        this.students.pop;
        this.router.navigate(['/studentslist'])
      }
    )

  }

  ngOnInit() {
  }

}
