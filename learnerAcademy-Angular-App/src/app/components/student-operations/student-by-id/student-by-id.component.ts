import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/service/student.service';
import { Student } from 'src/app/AppModel/student';



@Component({
  selector: 'app-student-by-id',
  templateUrl: './student-by-id.component.html',
  styleUrls: ['./student-by-id.component.css']
})
export class StudentByIdComponent implements OnInit {

private student:Student;
private students:Student[] = [];
private id:number;

  constructor( private service:StudentService) {

  }

  public getStudentById(){
    this.students=[];
    this.service.getStudentById(this.id).subscribe(
      data=>{
        this.student=data;
        this.students.push(this.student)
      }
    )
  }



  ngOnInit() {
  }

}
