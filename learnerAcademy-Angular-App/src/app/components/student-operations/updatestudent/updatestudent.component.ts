import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/service/student.service';
import { Router } from '@angular/router';
import { Student } from 'src/app/AppModel/student';

@Component({
  selector: 'app-updatestudent',
  templateUrl: './updatestudent.component.html',
  styleUrls: ['./updatestudent.component.css']
})
export class UpdatestudentComponent implements OnInit {
private student: Student;

  constructor(private service:StudentService, private route:Router) { 

    this.student=new Student();
  }

  public updateStudent(){
    
    return this.service.updateStudent(this.student).subscribe(
      data=>{this.student= new Student();
        this.route.navigate(['/studentslist'])


      }
    )
  }

  ngOnInit() {
  }

}
