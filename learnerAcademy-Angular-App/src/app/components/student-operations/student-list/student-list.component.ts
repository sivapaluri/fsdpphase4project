import { Component, OnInit } from '@angular/core';
import { Student } from 'src/app/AppModel/student';
import { StudentService } from 'src/app/service/student.service';
import { ClassService } from 'src/app/service/class.service';


@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {

  private students:Student[];
  
  
  constructor(private stdservice:StudentService, private clsservice:ClassService) {

   }

  ngOnInit() {
    this.stdservice.getAllStudents().subscribe(data=>{
      this.students=data;
      console.log(data);
    })
  }

}
