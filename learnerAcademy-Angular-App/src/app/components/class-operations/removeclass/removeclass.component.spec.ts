import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveclassComponent } from './removeclass.component';

describe('RemoveclassComponent', () => {
  let component: RemoveclassComponent;
  let fixture: ComponentFixture<RemoveclassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemoveclassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveclassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
