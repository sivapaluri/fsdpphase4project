import { Component, OnInit } from '@angular/core';
import { ClassService } from 'src/app/service/class.service';
import { Router } from '@angular/router';
import { ClassDetail } from 'src/app/AppModel/classdetail';
import { Student } from 'src/app/AppModel/student';
import { StudentService } from 'src/app/service/student.service';

@Component({
  selector: 'app-addclass',
  templateUrl: './addclass.component.html',
  styleUrls: ['./addclass.component.css']
})
export class AddclassComponent implements OnInit {
private classdetail:ClassDetail;


  
   constructor(private service:ClassService, private router:Router){  
  this.classdetail= new ClassDetail();
  
  }

  public createClass(){

this.service.createClass(this.classdetail).subscribe(data=>{
      this.classdetail= new ClassDetail();
      console.log(data);

      
   this.router.navigate(['/classlist'])
    })
 
  }

  ngOnInit() {
  }

}
