import { Component, OnInit, ɵConsole } from '@angular/core';
import { ClassService } from 'src/app/service/class.service';
import { ClassDetail } from 'src/app/AppModel/classdetail';
import { StudentService } from 'src/app/service/student.service';
import { Student } from 'src/app/AppModel/student';


@Component({
  selector: 'app-classlist',
  templateUrl: './classlist.component.html',
  styleUrls: ['./classlist.component.css']
})
export class ClasslistComponent implements OnInit {


  private classes: ClassDetail[];
  constructor(private service: ClassService) { }

  ngOnInit() {

    this.service.getAllclasses().subscribe(data => {
      this.classes = data;
      console.log(data);
      
    });
  }
}

