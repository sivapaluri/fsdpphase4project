import { Component, OnInit } from '@angular/core';
import { ClassService } from 'src/app/service/class.service';
import { ClassDetail } from 'src/app/AppModel/classdetail';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updateclass',
  templateUrl: './updateclass.component.html',
  styleUrls: ['./updateclass.component.css']
})
export class UpdateclassComponent implements OnInit {

 classdetail:ClassDetail;

  constructor(private service:ClassService,private route:Router) {
    this.classdetail = new ClassDetail;
   }

   public updateClass(){
     return this.service.updateClass(this.classdetail).subscribe(data=>{
       this.classdetail= new ClassDetail();
       this.route.navigate(['/classlist'])
     })  }

  ngOnInit() {
  }

}
