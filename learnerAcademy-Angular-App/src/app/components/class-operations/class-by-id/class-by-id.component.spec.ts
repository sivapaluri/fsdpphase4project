import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassByIdComponent } from './class-by-id.component';

describe('ClassByIdComponent', () => {
  let component: ClassByIdComponent;
  let fixture: ComponentFixture<ClassByIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassByIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassByIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
