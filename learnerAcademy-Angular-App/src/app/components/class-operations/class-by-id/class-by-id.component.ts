import { Component, OnInit } from '@angular/core';
import { ClassService } from 'src/app/service/class.service';
import { ClassDetail } from 'src/app/AppModel/classdetail';

@Component({
  selector: 'app-class-by-id',
  templateUrl: './class-by-id.component.html',
  styleUrls: ['./class-by-id.component.css']
})
export class ClassByIdComponent implements OnInit {
   classes:ClassDetail[]=[];
 classId:number;
 cls:ClassDetail;
  constructor(private service:ClassService) { }

  public getClassById(){
    this.classes=[];
    this.service.getClassById(this.classId).subscribe(
      data=>{
        this.cls=data;
        this.classes.push(this.cls)
        console.log(data);
      }
    )
  }

  ngOnInit() {
  }

}
