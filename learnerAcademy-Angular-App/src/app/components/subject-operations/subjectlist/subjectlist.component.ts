import { Component, OnInit } from '@angular/core';
import { Subject } from 'src/app/AppModel/subject';
import { SubjectService } from 'src/app/service/subject.service';



@Component({
  selector: 'app-subjectlist',
  templateUrl: './subjectlist.component.html',
  styleUrls: ['./subjectlist.component.css']
})
export class SubjectlistComponent implements OnInit {

  private subjects:Subject[];
  constructor(private service:SubjectService) { }

  ngOnInit() {
    this.service.getAllsubjects().subscribe(data=>{
      this.subjects=data;
    })
  }

}
