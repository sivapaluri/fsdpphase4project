import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemovesubjectComponent } from './removesubject.component';

describe('RemovesubjectComponent', () => {
  let component: RemovesubjectComponent;
  let fixture: ComponentFixture<RemovesubjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RemovesubjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RemovesubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
