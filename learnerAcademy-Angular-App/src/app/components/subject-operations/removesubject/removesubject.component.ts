import { Component, OnInit } from '@angular/core';
import { Subject } from 'src/app/AppModel/subject';
import { SubjectService } from 'src/app/service/subject.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-removesubject',
  templateUrl: './removesubject.component.html',
  styleUrls: ['./removesubject.component.css']
})
export class RemovesubjectComponent implements OnInit {

  private subject:Subject;
  private subjects:Subject[] = [];
  private sNo:number;
  constructor(private service:SubjectService,private router:Router) { }


  public removeSubject(){

    return this.service.removeSubject(this.sNo).subscribe(
      data=>{
        this.subject=data;
        this.subjects.pop;
        this.router.navigate(['/subjectlist'])
      }
    )

  }

  ngOnInit() {
  }

}
