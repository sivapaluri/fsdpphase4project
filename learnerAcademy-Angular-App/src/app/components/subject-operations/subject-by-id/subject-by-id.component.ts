import { Component, OnInit } from '@angular/core';
import { Subject } from 'src/app/AppModel/subject';
import { SubjectService } from 'src/app/service/subject.service';

@Component({
  selector: 'app-subject-by-id',
  templateUrl: './subject-by-id.component.html',
  styleUrls: ['./subject-by-id.component.css']
})
export class SubjectByIdComponent implements OnInit {
  private subject:Subject;
  private subjects:Subject[] = [];
  private sNo:number;
  constructor(private service:SubjectService) { }

  public getSubjectById(){
    this.subjects=[];
    this.service.getSubjectById(this.sNo).subscribe(
      data=>{
        this.subject=data;
        this.subjects.push(this.subject)
      }
    )
  }

  ngOnInit() {
  }

}
