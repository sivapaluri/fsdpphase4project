import { Component, OnInit } from '@angular/core';
import { Subject } from 'src/app/AppModel/subject';
import { Router } from '@angular/router';
import { SubjectService } from 'src/app/service/subject.service';

@Component({
  selector: 'app-updatesubject',
  templateUrl: './updatesubject.component.html',
  styleUrls: ['./updatesubject.component.css']
})
export class UpdatesubjectComponent implements OnInit {

  subject:Subject;
  constructor(private service:SubjectService,private router:Router) { 

    this.subject=new Subject();
  }

  updateSubject(){
    this.service.updateSubject(this.subject).subscribe(data=>{
      this.subject=new Subject();
      this.router.navigate(['/subjectlist'])
    })
  }

  ngOnInit() {
  }

}
