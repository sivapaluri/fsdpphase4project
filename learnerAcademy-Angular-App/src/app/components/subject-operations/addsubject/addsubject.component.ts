import { Component, OnInit } from '@angular/core';
import { SubjectService } from 'src/app/service/subject.service';
import { Router } from '@angular/router';
import { Subject } from 'src/app/AppModel/subject';


@Component({
  selector: 'app-addsubject',
  templateUrl: './addsubject.component.html',
  styleUrls: ['./addsubject.component.css']
})
export class AddsubjectComponent implements OnInit {
subject:Subject;
  constructor(private service:SubjectService,private router:Router) { 

    this.subject= new Subject();
  }

createSubject(){
  this.service.createSubject(this.subject).subscribe(data=>{
    this.subject= new Subject();
    this.router.navigate(['/subjectlist'])
  })
}

  ngOnInit() {
  }

}
