import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Student } from '../AppModel/student';
import { ClassDetail } from '../AppModel/classdetail';



@Injectable({
  providedIn: 'root'
})
export class StudentService {

  private url:string;

  constructor(private http:HttpClient ) {
    this.url="http://localhost:8080/Learner_Academy/studentdetail";
   }

   public getAllStudents():Observable<Student[]>{
    return this.http.get<Student[]>(this.url);
  }

  public createStudent(student:Student){
    return this.http.post<Student>(this.url,student);
  }

  public getStudentById(id:number):Observable<Student>{
    return this.http.get<Student>("http://localhost:8080/Learner_Academy/studentdetail/" + id);
  }

  public updateStudent(student:Student){
    return this.http.put<Student>("http://localhost:8080/Learner_Academy/studentdetail",student);
  }
  public deleteStudent(id:number){
    return this.http.delete<Student>("http://localhost:8080/Learner_Academy/studentdetail/"+ id)
  }
}
