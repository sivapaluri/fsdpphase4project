import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ClassDetail } from '../AppModel/classdetail';

@Injectable({
  providedIn: 'root'
})
export class ClassService {

  private url:string;

  constructor(private http:HttpClient ) {
    this.url="http://localhost:8080/Learner_Academy/classdetail";
   }

   public getAllclasses():Observable<ClassDetail[]>{
    return this.http.get<ClassDetail[]>(this.url);
  }

  public createClass(classdetail:ClassDetail){
    return this.http.post<ClassDetail>(this.url,classdetail);
  }

  public getClassById(id:number):Observable<ClassDetail>{
    return this.http.get<ClassDetail>("http://localhost:8080/Learner_Academy/classdetail/" + id);
  }

  public updateClass(classdetail:ClassDetail){
    return this.http.put<ClassDetail>("http://localhost:8080/Learner_Academy/classdetail",classdetail);
  }
  public deleteClass(id:number){
    return this.http.delete<ClassDetail>("http://localhost:8080/Learner_Academy/classdetail/"+ id)
  }
}
