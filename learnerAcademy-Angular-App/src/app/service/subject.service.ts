import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subject } from '../AppModel/subject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {

  private url:string;

  constructor(private http:HttpClient ) {
    this.url="http://localhost:8080/Learner_Academy/subjectdetail";
   }

   public getAllsubjects():Observable<Subject[]>{
    return this.http.get<Subject[]>(this.url);
  }

  public createSubject(subject:Subject){
    return this.http.post<Subject>(this.url,subject);
  }

  public updateSubject(subject:Subject){
    return this.http.put<Subject>("http://localhost:8080/Learner_Academy/subjectdetail/",subject);
  }

  public removeSubject(id:number){
    return this.http.delete<Subject>("http://localhost:8080/Learner_Academy/subjectdetail/" + id);
  }

  public getSubjectById(id:number):Observable<Subject>{
    return this.http.get<Subject>("http://localhost:8080/Learner_Academy/subjectdetail/" + id);
  }
}

