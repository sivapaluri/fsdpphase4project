import { Injectable } from '@angular/core';
import { Teacher } from '../AppModel/teacher';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  private url:string;

  constructor(private http:HttpClient ) {
    this.url="http://localhost:8080/Learner_Academy/teacherdetail";
   }

   public getAllteachers():Observable<Teacher[]>{
    return this.http.get<Teacher[]>(this.url);
  }

  public getTeacherById(id:number):Observable<Teacher>{
    return this.http.get<Teacher>("http://localhost:8080/Learner_Academy/teacherdetail/"+ id);
  }

  public createTeacher(teacher:Teacher){
    return this.http.post<Teacher>(this.url,teacher);
  }

  public updateTeacher(teacher:Teacher){
    return this.http.put<Teacher>(this.url,teacher);
  }

  public deleteTeacher(id:number){
    return this.http.delete<Teacher>("http://localhost:8080/Learner_Academy/teacherdetail/"+ id);
  }
}
