import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutusComponent } from './components/aboutus/aboutus.component';
import { HomeComponent } from './components/home/home.component';
import { VisionComponent } from './components/vision/vision.component';
import { MissionComponent } from './components/mission/mission.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { AdmintasksComponent } from './components/admintasks/admintasks.component';
import { StudentTaskComponent } from './components/student-task/student-task.component';
import { StudentListComponent } from './components/student-operations/student-list/student-list.component';
import { AddstudentComponent } from './components/student-operations/addstudent/addstudent.component';
import { RemovestudentComponent } from './components/student-operations/removestudent/removestudent.component';
import { StudentByIdComponent } from './components/student-operations/student-by-id/student-by-id.component';
import { UpdatestudentComponent } from './components/student-operations/updatestudent/updatestudent.component';
import { ClassTaskComponent } from './components/class-task/class-task/class-task.component';
import { AddclassComponent } from './components/class-operations/addclass/addclass.component';
import { UpdateclassComponent } from './components/class-operations/updateclass/updateclass.component';
import { ClasslistComponent } from './components/class-operations/classlist/classlist.component';
import { RemoveclassComponent } from './components/class-operations/removeclass/removeclass.component';
import { ClassByIdComponent } from './components/class-operations/class-by-id/class-by-id.component';
import { TeacherTaskComponent } from './components/teacher-task/teacher-task/teacher-task.component';
import { AddteacherComponent } from './components/teacher-operations/addteacher/addteacher.component';
import { UpdateteacherComponent } from './components/teacher-operations/updateteacher/updateteacher.component';
import { TeacherlistComponent } from './components/teacher-operations/teacherlist/teacherlist.component';
import { RemoveteacherComponent } from './components/teacher-operations/removeteacher/removeteacher.component';
import { TeacherByIdComponent } from './components/teacher-operations/teacher-by-id/teacher-by-id.component';
import { SubjectTaskComponent } from './components/subject-task/subject-task/subject-task.component';
import { AddsubjectComponent } from './components/subject-operations/addsubject/addsubject.component';
import { UpdatesubjectComponent } from './components/subject-operations/updatesubject/updatesubject.component';
import { RemovesubjectComponent } from './components/subject-operations/removesubject/removesubject.component';
import { SubjectlistComponent } from './components/subject-operations/subjectlist/subjectlist.component';
import { SubjectByIdComponent } from './components/subject-operations/subject-by-id/subject-by-id.component';





const routes: Routes = [
  {path:"aboutus",component:AboutusComponent},
  {path:"home",component:HomeComponent},
  {path:"vision",component:VisionComponent},
  {path:"mission",component:MissionComponent},
  {path:"testimonials",component:TestimonialsComponent},
  {path:"admintasks",component:AdmintasksComponent},
  {path:"student",component:StudentTaskComponent},
  {path:"studentslist",component:StudentListComponent},
  {path:"addstudent",component:AddstudentComponent},
  {path:"removestudent",component:RemovestudentComponent},
  {path:"studentbyid",component:StudentByIdComponent},
  {path: "updatestudent",component:UpdatestudentComponent},
  {path: "class",component:ClassTaskComponent},
  {path: "addclass",component:AddclassComponent},
  {path: "updateclass",component:UpdateclassComponent},
  {path: "removeclass",component:RemoveclassComponent},
  {path: "classlist",component:ClasslistComponent},
  {path: "classbyid",component:ClassByIdComponent},
  {path: "teacher",component:TeacherTaskComponent},
  {path: "addteacher",component:AddteacherComponent},
  {path: "updateteacher",component:UpdateteacherComponent},
  {path: "removeteacher",component:RemoveteacherComponent},
  {path: "teacherlist",component:TeacherlistComponent},
  {path: "teacherbyid",component:TeacherByIdComponent},
  {path: "subject",component:SubjectTaskComponent},
  {path: "addsubject",component:AddsubjectComponent},
  {path: "updatesubject",component:UpdatesubjectComponent},
  {path: "removesubject",component:RemovesubjectComponent},
  {path: "subjectlist",component:SubjectlistComponent},
  {path: "subjectbyid",component:SubjectByIdComponent},
  {path:"teacherlist",component:TeacherlistComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
